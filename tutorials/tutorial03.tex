\documentclass[12pt]{book}
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{tutorial}
\usepackage{listings}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\usepackage{multicol}
\usepackage{etoolbox}
\usepackage{tikz}
\usepackage{enumerate}
\usepackage{fancyhdr}
\usepackage[letterpaper, margin=1in]{geometry}

\pagestyle{fancy}
\lhead{CSC373H1Y (Summer 2019)}
\chead{Tutorial 3}
\rhead{Lily Li}

\makeatletter
\patchcmd{\chapter}{\if@openright\cleardoublepage\else\clearpage\fi}{}{}{}
\makeatother
\Scribe{Rupert Wu}
\TA{Lily Li}
\TutorialNumber{3}
\TutorialDate{2019/05/23}
\TutorialTitle{Dynamic Programming}
\lstset{style=mystyle}

\begin{document}
	\MakeScribeTop

\section*{Problem 1: Max-Block}

\begin{multicols}{2}
    \subsubsection*{Input}
    $$M \in \{0,1\}^{n \times n}$$
    \subsubsection*{Output}
    Size of the maximum \textbf{square} in $M$ containing the same entry type.
    \subsubsection*{Example}
    Consider the grid to the left with $n=5$.
    \begin{center}
        \begin{tikzpicture}
            \draw[step=1cm,color=gray] (0,0) grid (5,5);
            \node at (0.5,0.5) {$1$};
            \node at (0.5,2.5) {$1$};
            \node at (0.5,3.5) {$1$};
            \node at (1.5,0.5) {$1$};
            \node at (1.5,1.5) {$1$};
            \node at (1.5,2.5) {$1$};
            \node at (1.5,3.5) {$1$};
            \node at (1.5,4.5) {$1$};
            \node at (2.5,2.5) {$1$};
            \node at (2.5,3.5) {$1$};
            \node at (3.5,1.5) {$1$};
            \node at (4.5,3.5) {$1$};
        \end{tikzpicture}
    \end{center}
\end{multicols}

\subsection*{Strategy}
Let the square $M_{i,j}$ be the bottom-right corner of a compound square. We can check if the above, above-left and left squares are equal in value to $M_{i,j}$.
\begin{align*}
    E(i,j) &:= M_{i, j} = M_{i-1,j} = M_{i, j-1} = M_{i-1, j-1}\\
    S(i,j) &:= min(A(i,j-1)-1, A(i-1,j)-1, A(i-1, j-1)) + 1
\end{align*}
We can then find the size of the compound square with $M_{i,j}$ as its bottom-right corner.
$$A(i,j) := \begin{Bmatrix}
    E(i,j) &\implies& S(i,j) + 1\\
    otherwise &\implies& 1
\end{Bmatrix}$$
\subsubsection*{Base Case}
$$i == 1 \lor j == 1 \implies A(i,j) = 1$$
\subsubsection*{Observation}
$$\max_{i,j} a_{i,j} = A(i,j)$$

\subsection*{Maximum Square}
Putting together the pieces of this recursive strategy, we can construct an (iterative) algorithm.\\

\begin{algorithm}[H]
    \SetAlgoLined
    $OPT := 0$\;
    $A := zer\mathcal{O}(n,n)$\;
    \For{$i := 1,...,n$}{
        \For{$j := 1,...,n$}{
            \uIf{$ i==1 \lor j==1$}{
                $OPT = 1$\;
            }\uElseIf{$M_{i, j} = M_{i-1,j} = M_{i, j-1} = M_{i-1, j-1}$}{
                $OPT = min(A(i,j-1)-1, A(i-1,j)-1, A(i-1, j-1)) + 1$\;
            }\Else{
                $OPT = 1$\;
            }
            $A(i,j) = OPT$\;
        }
    }
    return $\max_A a_{i,j}$\;
    \caption{\textsc{IterativeMaxBlock}($M$, $n$): $\Theta(n^2)$}
\end{algorithm}

\newpage
\section*{Problem 2: Edit-Distance [CLRS $15-5$]}
\begin{multicols}{2}
    In order to transform one source string of text $x[1,...m]$ to a target string $y[1,...,n]$ , we can perform various transformation operations $OP$. Each operation $OP$ has a cost $\textsc{C}(Ops.OP)$, which we want to minimize.
    \subsubsection*{Input}
    $$\sum := \{a,b,...,y,z\}$$
    $$x \in \sum {}^m \qquad y \in \sum {}^n$$
    \subsubsection*{Example}
    $$m = 5, n = 7$$
    \begin{align*}
    &\begin{tikzpicture}
        \draw[step=1cm,color=gray] (0,0) grid (5,1);
        \node at (-0.5,0.5) {x:};
        \node at (2.5,0.5) {i};
    \end{tikzpicture}\\
    &\begin{tikzpicture}
        \draw[step=1cm,color=gray] (0,0) grid (7,1);
        \node at (-0.5,0.5) {y:};
        \node at (2.5,0.5) {j};
    \end{tikzpicture}
    \end{align*}

    \subsubsection*{Operations (Ops)}
    \begin{algorithm}[H]
        \SetAlgoLined
        \caption{\textsc{Copy}}
        $Z[j] = x[i]$\;
        $i++$; \quad $j++$\;
    \end{algorithm}
    \begin{algorithm}[H]
        \SetAlgoLined
        \caption{\textsc{Replace}}
        $Z[j] = c \mid c \in \sum$\;
        $i++$; \quad $j++$\;
    \end{algorithm}
    \begin{algorithm}[H]
        \SetAlgoLined
        \caption{\textsc{Delete}}
        $i++$\;
    \end{algorithm}
    \begin{algorithm}[H]
        \SetAlgoLined
        \caption{\textsc{Insert}}
        $Z[j] = c \mid c \in \sum$\;
        $j++$\;
    \end{algorithm}
    \begin{algorithm}[H]
        \SetAlgoLined
        \caption{\textsc{Twiddle}}
        $Z[j] = x[i+1]$\;
        $Z[j+1] = x[i]$\;
        $i += 2$; \quad $j += 2$\;
    \end{algorithm}
    \begin{algorithm}[H]
        \SetAlgoLined
        \caption{\textsc{Kill}}
        $i = m+1$\;
    \end{algorithm}
\end{multicols}

\subsection*{Example}
$$x := \text{"saladbar"} \qquad y := \text{"island"}$$

\subsubsection*{Optimal Solution}
\begin{center}\begin{tabular}{ |c|c|c| }
    \hline
    Operation & $x$ & $y$\\
    \hline
    \textsc{Init} & \qquad \underline{s}aladbar & \qquad \underline{ }\\
    \textsc{Insert}(i) & \qquad \underline{s}aladbar & \qquad i\underline{ }\\
    \textsc{Copy} & \qquad s\underline{a}ladbar & \qquad is\underline{ }\\
    \textsc{Twiddle} & \qquad sala\underline{d}bar & \qquad isla\underline{ }\\
    \textsc{Insert}(n) & \qquad sala\underline{d}bar & \qquad islan\underline{ }\\
    \textsc{Delete} & \qquad salad\underline{b}ar & \qquad island\underline{ }\\
    \textsc{Kill} & \qquad saladbar\underline{ } & \qquad island\underline{ }\\
    \hline
\end{tabular}\end{center}

\subsection*{Optimal (Minimal) Cost}
\begin{algorithm}[H]
    \SetAlgoLined
    \caption{\textsc{OPT}($x$, $m$, $y$, $n$, $Ops$, $OP$): }
    \uIf{$j == n+1$}{
        return $\textsc{C}(Ops.Kill)$\;
    }\uIf{$i == m+1 \land j \leq n$}{
        return $\infty$\;
    }
    $val := 0$\;
    $val = OPT(i+1, j+1) + \textsc{C}(Ops.Replace)$\;
    $val = \min{(OPT(i+1,j) + \textsc{C}(Ops.Delete), val)}$\;
    $val = \min{(OPT(i,j+1) + \textsc{C}(Ops.Insert), val)}$\;
    \Switch{OP}{
        \uCase{Copy}{
            \uIf{$x_i == y_j$}{
                $val = \min(OPT(i+1,j+1) + \textsc{C}(Ops.Copy), val)$
            }
        }\uCase{Twiddle}{
            \uIf{$x_i == y_j$}{
                $val = \min(OPT(i+1,j+1) + \textsc{C}(Ops.Twiddle), val)$
            }
        }
    }
    return $val$\;
\end{algorithm}

\end{document}