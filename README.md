# CSC373H1Y Summer 2019

These are course resources from CSC373H1Y (Summer 2019) taught by Koushik Pal for the Faculty of Arts & Science at the University of Toronto.

## Important

- Aug22: Final Exam (14:00-17:00)

## Contents

- Scribed notes from lectures 1 through 6.
- The instructor's lecture notes are avaiable from lectures 3 through 12.
